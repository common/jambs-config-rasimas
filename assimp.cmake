SET(assimp_repo_type git)
SET(assimp_repo_url "https://github.com/assimp/assimp")
SET(assimp_repo_tag "master")
SET(assimp_extra_config_params -DASSIMP_DEBUG_POSTFIX:STRING=)

SET(assimp_depends)
